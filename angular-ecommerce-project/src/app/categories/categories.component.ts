import { Component, OnInit,Input } from '@angular/core';
import { Product } from '../models/products.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories:Product[];

  constructor() { 
    this.categories=[]
  }

  ngOnInit(): void {
  }

  agregar(nombre:string, url:string, description:string):boolean {
    this.categories.push(new Product(nombre,url,description));
  
    return false 
  }
}
